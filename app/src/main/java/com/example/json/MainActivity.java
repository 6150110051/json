package com.example.json;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "jsonparsing";
    // Progress Dialog
    private ProgressDialog pDialog;
    private ListView listView = null;
    private ArrayAdapter arrayAdapter = null;
    private String[] blogTitles;
    boolean status = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listView);
// for JSON object data
        //final String url = "https://androidexample.com/media/webservice/JsonReturn.php";
//final String url = "https://api.androidhive.info/contacts/”;
//final String url = "https://jsonplaceholder.typicode.com/users”;
        // for JSON array data
        final String url = "https://www.w3schools.com/js/customers_mysql.php";
        status = checkNetworkConenction();
        if (status) {
            Toast.makeText(getApplicationContext(), "Network Available", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Network Not Available", Toast.LENGTH_LONG).show();
        }
        new AsyncHttpTask().execute(url);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
// ListView Clicked item index
                int itemPosition = position;
// ListView Clicked item value
                String itemValue = (String) listView.getItemAtPosition(position);
// Show Alert
                Toast.makeText(getApplicationContext(),
                        "Position :" + itemPosition + " ListItem : " + itemValue, Toast.LENGTH_SHORT)
                        .show();

            }
        });
    }
    public final boolean checkNetworkConenction() {
        ConnectivityManager ConnectionManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
//Network Available
            return true;
        } else {
// Network Not Available
            return false;
        }
    }
    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setTitle("Please Wait..");
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected Integer doInBackground(String... params) {
            InputStream inputStream = null;
            HttpURLConnection urlConnection = null;
            Integer result = 0;
            try {
                /* forming th java.net.URL object */
                URL url = new URL(params[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                /* optional request header */
                urlConnection.setRequestProperty("Content-Type", "application/json");
                /* optional request header */
                urlConnection.setRequestProperty("Accept", "application/json");
                /* for Get request */
                urlConnection.setRequestMethod("GET");
                int statusCode = urlConnection.getResponseCode();
                /* 200 represents HTTP OK */

                if (statusCode == 200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    String response = convertInputStreamToString(inputStream);
                    parseResult(response);
                    result = 1; // Successful
                } else {
                    result = 0; //"Failed to fetch data!";
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result; //"Failed to fetch data!";
        }
        @Override
        protected void onPostExecute(Integer result) {
// Close progress dialog
            pDialog.dismiss();
            /* Download complete. Lets update UI */
            if (result == 1) {
                arrayAdapter = new ArrayAdapter(getApplicationContext(), R.layout.custom_textview, R.id.tv_list,
                        blogTitles);
                listView.setAdapter(arrayAdapter);
            } else {
                Log.e(TAG, "Failed to fetch data!");
            }
        }
        private String convertInputStreamToString(InputStream inputStream) throws IOException {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while ((line = bufferedReader.readLine()) != null) {
                result += line;
            }
            /* Close Stream */
            if (null != inputStream) {
                inputStream.close();
            }
            return result;
        }

        private void parseResult(String result) {
            try {
                //for JSON Array data
                JSONArray posts = new JSONArray(result);
                blogTitles = new String[posts.length()];
                for (int i = 0; i < posts.length(); i++) {
                    JSONObject post = posts.optJSONObject(i);
                    //for JSON Array data
                    String title = post.optString("Name");
                    blogTitles[i] = title;
                }
            } catch (
                    JSONException e
            )
            {
                e.printStackTrace();
            }
        }
    }
}